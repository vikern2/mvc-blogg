﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    let loadComments = function (postId) {
        var commentsList = $("#list");

        $.ajax({
            dataType: "json",
            url: "https://localhost:44308/api/CommentsApi/" + postId,
            data: [],
            success: function (response) {
                commentsList.empty();

                for (let i = 0; i < response.length; i++) {
                    let comment = response[i];
                    commentsList.append("<li>" + comment["author"] + " : " + comment["content"] + " : " + comment["posted"] + "</li>");
                }
            }
        });
    }

    let postId = $('#PostId').val();
    loadComments(postId);

    $('#commentForm').submit(function (e) {
        let postId = $('#PostId').val();
        let content = $('#Content').val();
        let author = $('#Author').val();
        let jwt = $('#jwt').val();

        if (postId.length == 0) {
            alert('Something behind the curtains has gone wrong. Reload and try again.');
            return e.preventDefault();
        }

        if (content.length < 3) {
            alert('Comment can\'t be less than 3 characters.');
            return e.preventDefault();
        }

        $.ajax({
            url: '/api/CommentsApi',
            method: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({
                'jwt': jwt,
                'comment': {
                    'commentId': 0,
                    'author': author,
                    'postId': postId,
                    'content': content,
                    'post': null,
                    'owner': null
                }
            }),
            success: function (response, status, xhr) {
                if (response == 'Done') {
                    loadComments(postId);
                } else {
                    alert(status);
                }
            }
        });
        e.preventDefault();
    });
});