﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogg.Models
{
    public class BlogViewModel
    {
        public String Owner { get; set; }
        public String Name { get; set; }
        public int CommentCount { get; set; }
        public int BlogId { get; set; }
    }
}
