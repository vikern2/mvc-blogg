﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogg.Models
{
    public class SameUserHandler : AuthorizationHandler<OperationAuthorizationRequirement, Blog>
    {
        UserManager<ApplicationUser> manager;

        public SameUserHandler(UserManager<ApplicationUser> manager)
        {
            this.manager = manager;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, Blog resource)
        {
            if(context.User == null || resource == null)
            {
                return Task.CompletedTask;
            }

            if(resource.ownerUser.Id == manager.GetUserId(context.User))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
