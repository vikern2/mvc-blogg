﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogg.Models
{
    public class Comment
    {
        public int CommentId { get; set; }
        public String Author { get; set; }
        public String Content { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
        public ApplicationUser owner { get; set; }
        public DateTime posted { get; set; }
    }
}
