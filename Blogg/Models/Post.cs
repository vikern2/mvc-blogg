﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogg.Models
{
    public class Post
    {
        public int PostId { get; set; }
        public String Title { get; set; }
        public String Content { get; set; }
        public int BlogId { get; set; }
        public Blog Blog { get; set; }
        public ApplicationUser owner { get; set; }
        public int CommentCount { get; set; }
        public DateTime LastComment { get; set; }
    }
}
