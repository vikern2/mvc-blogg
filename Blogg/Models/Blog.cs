﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogg.Models
{
    public class Blog
    {
        public int BlogId { get; set; }
        public String Url { get; set; }
        public String Owner { get; set; }
        public List<Post> Posts { get; set; }
        public String Name { get; set; }
        public Boolean openForNewPostsAndComments { get; set; }
        public virtual ApplicationUser ownerUser { get; set; }

    }
}
