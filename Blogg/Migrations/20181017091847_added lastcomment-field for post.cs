﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Blogg.Migrations
{
    public partial class addedlastcommentfieldforpost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastComment",
                table: "posts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastComment",
                table: "posts");
        }
    }
}
