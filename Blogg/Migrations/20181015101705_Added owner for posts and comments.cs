﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Blogg.Migrations
{
    public partial class Addedownerforpostsandcomments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ownerId",
                table: "posts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ownerId",
                table: "Comment",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_posts_ownerId",
                table: "posts",
                column: "ownerId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_ownerId",
                table: "Comment",
                column: "ownerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_AspNetUsers_ownerId",
                table: "Comment",
                column: "ownerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_posts_AspNetUsers_ownerId",
                table: "posts",
                column: "ownerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_AspNetUsers_ownerId",
                table: "Comment");

            migrationBuilder.DropForeignKey(
                name: "FK_posts_AspNetUsers_ownerId",
                table: "posts");

            migrationBuilder.DropIndex(
                name: "IX_posts_ownerId",
                table: "posts");

            migrationBuilder.DropIndex(
                name: "IX_Comment_ownerId",
                table: "Comment");

            migrationBuilder.DropColumn(
                name: "ownerId",
                table: "posts");

            migrationBuilder.DropColumn(
                name: "ownerId",
                table: "Comment");
        }
    }
}
