﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Blogg.Migrations
{
    public partial class addedcommentcountfieldforpost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CommentCount",
                table: "posts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "posted",
                table: "Comment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommentCount",
                table: "posts");

            migrationBuilder.DropColumn(
                name: "posted",
                table: "Comment");
        }
    }
}
