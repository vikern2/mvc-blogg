﻿using Blogg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Blogg.Models
{
    public interface IRepository
    {
        IEnumerable<Blog> getall();
        Task createAsync(Blog blog, ClaimsPrincipal claimsPrincipal);
        void delete(int id);
        void update(Blog blog);
        Blog getBlog(int? id);
        Boolean isOpen(int? id);
        int CountComments(int? id);
    }
}
