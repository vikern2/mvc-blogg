﻿using Blogg.Models;
using Blogg.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogg.Repository;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;

namespace Blogg.Repository
{
    public class BlogRepository : IRepository
    {
        private ApplicationDbContext db;
        private UserManager<ApplicationUser> manager;
        public BlogRepository(UserManager<ApplicationUser> manager, ApplicationDbContext database)
        {
            this.manager = manager;
            this.db = database;
        }

        public int CountComments(int? id)
        {
            int count = 0;
            IEnumerable<Post> posts = db.posts.Where(post => post.BlogId == id);
            for(int i = 0; i<posts.Count(); i++)
            {
                count += posts.ElementAt(i).CommentCount;
            }
            return count;
        }

        public async Task createAsync(Blog blog, ClaimsPrincipal userClaims)
        {
            var current = await manager.GetUserAsync(userClaims);
            blog.ownerUser = current;
            db.Add(blog);
            db.SaveChanges();
        }

        public void delete(int id)
        {           
            db.blogs.Remove(getBlog(id));
            PostRepository pr = new PostRepository(manager, db);
            IEnumerable<Post> posts = pr.getall(id);
            foreach(Post post in posts)
            {
                pr.Delete(post.PostId);
            }
            db.SaveChanges();
        }

        public IEnumerable<Blog> getall()
        {
            IEnumerable<Blog> blogs = db.blogs;
            return blogs;
        }

        public Blog getBlog(int? id)
        {
            return db.blogs.Include(x => x.ownerUser).Where(blog => blog.BlogId == id).First();
        }

        public bool isOpen(int? id)
        {
            return getBlog(id).openForNewPostsAndComments;
        }

        public void update(Blog blog)
        {
            db.blogs.Update(blog);
            db.SaveChangesAsync();
        }

     
    }
}
