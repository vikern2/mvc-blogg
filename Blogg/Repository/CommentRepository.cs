﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogg.Models;
using Blogg.Data;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Blogg.Repository
{
    public class CommentRepository : ICommentRepository
    {
        private ApplicationDbContext db;
        private UserManager<ApplicationUser> manager;
        public CommentRepository(UserManager<ApplicationUser> manager, ApplicationDbContext database)
        {
            this.manager = manager;
            this.db = database;
        }

        public async Task Create(Comment comment, ClaimsPrincipal userClaims)
        {
            var current = await manager.GetUserAsync(userClaims);
            comment.owner = current;
            comment.posted = DateTime.Now;
            db.Add(comment);
            ChangeCommentCount(comment.PostId, true);
            db.SaveChanges();
        }

        public void Delete(Comment comment)
        {
            db.Remove(comment);
            ChangeCommentCount(comment.PostId, false);
            db.SaveChanges();
        }

        public IEnumerable<Comment> getall(int? id)
        {
            IEnumerable<Comment> comments = db.Comment.Include(x => x.Post).Include(x => x.owner).Where(b => b.PostId == id).ToList();
            return comments;
        }

        public Comment getComment(int id)
        {
            return db.Comment.Include(x => x.owner).Where(comment => comment.CommentId == id).First();
        }

        public Post getPost(int? id)
        {
            return db.posts.Find(id);
        }

        public void Update(Comment comment)
        {
            db.Comment.Update(comment);
            db.SaveChanges();
        }

        public Blog getBlog(int? id)
        {
            return db.blogs.Find(id);
        }

        public void CreateApi(Comment comment)
        {
            db.Comment.Add(comment);
            ChangeCommentCount(comment.PostId, true);
            db.SaveChanges();
        }

        public void ChangeCommentCount(int? id, Boolean increase)
        {
            Post post = db.posts.Find(id);
            if (increase)
            {
                post.CommentCount++;
            }
            else
            {
                post.CommentCount--;
            }
            post.LastComment = DateTime.Now;
            db.posts.Update(post);
        }

        
    }
}
