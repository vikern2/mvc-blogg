﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogg.Models;
using Blogg.Data;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Blogg.Repository
{
    public class PostRepository : IPostRepository
    {
        private ApplicationDbContext db;
        private UserManager<ApplicationUser> manager;
        public PostRepository(UserManager<ApplicationUser> manager, ApplicationDbContext database)
        {
            this.manager = manager;
            this.db = database;
        }

        public async Task Create(Post post, ClaimsPrincipal userClaims)
        {
            var current = await manager.GetUserAsync(userClaims);
            post.owner = current;
            db.Add(post);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            db.Remove(getPost(id));
            CommentRepository cr = new CommentRepository(manager, db);
            IEnumerable<Comment> comments = cr.getall(id);
            foreach (Comment comment in comments)
            {
                cr.Delete(comment);
            }
            db.SaveChanges();
        }

        public IEnumerable<Post> getall(int? id)
        {
            IEnumerable<Post> posts = db.posts.Where(b => b.BlogId == id).ToList();
            
            foreach(Post post in posts)
            {
                post.Blog = db.blogs.Find(post.BlogId);
            }
            return posts;
        }

        public Blog getBlog(int? id)
        {
            return db.blogs.Find(id);
        }

        public Post getPost(int id)
        {
            return db.posts.Include(x => x.owner).Include(x => x.Blog).Where(post => post.PostId == id).First();
        }

        public void update(Post post)
        {
            db.posts.Update(post);
            db.SaveChanges();
        }
    }
}
