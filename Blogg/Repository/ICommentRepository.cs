﻿using Blogg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Blogg.Repository
{
    public interface ICommentRepository
    {
        Task Create(Comment comment, ClaimsPrincipal userClaims);
        Comment getComment(int id);
        IEnumerable<Comment> getall(int? id);
        void Update(Comment comment);
        void Delete(Comment comment);
        Post getPost(int? id);
        Blog getBlog(int? id);
        void CreateApi(Comment comment);
        void ChangeCommentCount(int? id, Boolean increase);
    }
}
