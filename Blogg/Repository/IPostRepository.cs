﻿using Blogg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Blogg.Repository
{
    public interface IPostRepository
    {
        Task Create(Post post, ClaimsPrincipal userClaims);
        void Delete(int id);
        Post getPost(int id);
        void update(Post post);
        IEnumerable<Post> getall(int? id);
        Blog getBlog(int? id);
    }
}
