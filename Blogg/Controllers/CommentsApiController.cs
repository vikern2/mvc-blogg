﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogg.Models;
using Blogg.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace Blogg.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsApiController : ControllerBase
    {
        private ICommentRepository repository;
        private UserManager<ApplicationUser> manager;
        public CommentsApiController(ICommentRepository repository, UserManager<ApplicationUser> manager)
        {
            this.repository = repository;
            this.manager = manager;
        }

        // GET: api/CommentsApi/5
        [HttpGet("{id}")]
        public IEnumerable<Comment> Get([FromRoute]int? id)
        {
            return repository.getall(id);
        }


        // POST: api/CommentsApi
        [HttpPost]
        public async Task PostAsync([FromBody]JObject json)
        {
            string jwt = (string)json["jwt"];
            Comment comment = new Comment
            {
                CommentId = 0,
                Author = (string)json["comment"]["author"],
                Content = (string)json["comment"]["content"],
                PostId = (int)json["comment"]["postId"],
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("long secret hehe asd");
            var tokenSecure = tokenHandler.ReadToken(jwt) as SecurityToken;
            var validations = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };

            var claims = tokenHandler.ValidateToken(jwt, validations, out tokenSecure);
            var userId = claims.Identity.Name;
            var user = await manager.FindByIdAsync(userId);

            comment.owner = user;
            comment.posted = DateTime.Now;

            repository.CreateApi(comment);
        }
    }
}
