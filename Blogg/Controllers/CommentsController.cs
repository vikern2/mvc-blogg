﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blogg.Data;
using Blogg.Models;
using Blogg.Repository;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Identity;
using System.Security.Principal;

namespace Blogg.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ICommentRepository _repository;
        private IAuthorizationService authorizationService;
        private UserManager<ApplicationUser> manager;
        public string latestJwt;

        public CommentsController(ICommentRepository rep, IAuthorizationService authorizationService, UserManager<ApplicationUser> manager)
        {
            this.authorizationService = authorizationService;
            _repository = rep;
            this.manager = manager;
        }

        // GET: Comments
        public async Task<IActionResult> Index(int? id)
        {
            ViewData["postId"] = id;
            ViewData["blogId"] = _repository.getPost(id).BlogId;
            ViewData["open"] = _repository.getBlog(_repository.getPost(id).BlogId).openForNewPostsAndComments;
            return View(_repository.getall(id));
        }

        // GET: Comments/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _repository.getComment(id);
            if (comment == null)
            {
                return NotFound();
            }
            ViewData["postId"] = comment.PostId;

            return View(comment);
        }

        [Authorize]
        // GET: Comments/Create
        public IActionResult Create(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("", "posts");
            }
            ViewData["postId"] = id;

            return View();
        }


        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CommentId,Author,Content,PostId")] Comment comment)
        {
            ClaimsPrincipal userClaims = this.User;

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.Create(comment, userClaims).Wait();
                    return RedirectToAction("Index/" + comment.PostId, "comments");
                }
                catch
                {
                    return View(comment);
                }
            }
            return View(comment);
        }

        // GET: Comments/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _repository.getComment(id);
            var isAuthorized = await authorizationService.AuthorizeAsync(User, comment, new OperationAuthorizationRequirement { Name = "edit" });

            if (!isAuthorized.Succeeded)
            {
                return View("Du må være innlogget på riktig bruker for å få tilgang til denne handlingen!");
            }

            if (comment == null)
            {
                return NotFound();
            }
            ViewData["postId"] = comment.PostId;
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CommentId,Author,Content,PostId")] Comment comment)
        {
            if (id != comment.CommentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.Update(comment);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CommentExists(comment.CommentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index/" + comment.PostId, "comments");
            }
            ViewData["postId"] = comment.PostId;
            return View(comment);
        }

        // GET: Comments/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _repository.getComment(id);
            var isAuthorized = await authorizationService.AuthorizeAsync(User, comment, new OperationAuthorizationRequirement { Name = "delete" });

            if (!isAuthorized.Succeeded)
            {
                return View("Du må være innlogget på riktig bruker for å få tilgang til denne handlingen!");
            }

            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            int postId = _repository.getComment(id).PostId;
            _repository.Delete(_repository.getComment(id));
            return RedirectToAction("Index/" + postId, "comments");
        }

        private bool CommentExists(int id)
        {
            return _repository.getComment(id) != null;
        }

        public ActionResult jquery(int id)
        {
            ViewData["postId"] = id;
            GenerateToken(User).Wait();
            ViewData["token"] = latestJwt;
            return View();
        }

        private async Task GenerateToken(IPrincipal principal)
        {
            var user = await manager.FindByNameAsync(principal.Identity.Name);
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("long secret hehe asd");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.Name, user.Id.ToString())
            }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            {

                var token = tokenHandler.CreateToken(tokenDescriptor);
                latestJwt = tokenHandler.WriteToken(token);
            }
        }
    }
}
