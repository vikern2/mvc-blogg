﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blogg.Repository;
using Blogg.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Blogg.Controllers
{
    public class PostsController : Controller
    {
        private IPostRepository _repository;
        private IAuthorizationService authorizationService;

        public PostsController(IPostRepository repository, IAuthorizationService authorizationService)
        {
            this.authorizationService = authorizationService;
            _repository = repository;
        }


        // GET: Posts
        public async Task<IActionResult> Index(int? id)
        {
            ViewData["blogId"] = id;
            ViewData["open"] = _repository.getBlog(id).openForNewPostsAndComments;
            return View(_repository.getall(id));
        }

        // GET: Posts/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _repository.getPost(id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        [Authorize]
        // GET: Posts/Create
        public IActionResult Create(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("", "blogs");
            }
            ViewData["blogId"] = id;

            return View();
        }

        [Authorize]
        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PostId,Title,Content,BlogId")] Post post)
        {
            ClaimsPrincipal userClaims = this.User; 

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.Create(post, userClaims).Wait();
                    return RedirectToAction("Index/" + post.BlogId, "posts");
                }
                catch
                {
                    return View(post);
                }
            }
            return View(post);
        }

        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _repository.getPost(id);

            var isAuthorized = await authorizationService.AuthorizeAsync(User, post, new OperationAuthorizationRequirement { Name = "edit" });

            if (!isAuthorized.Succeeded)
            {
                return View("Du må være innlogget på riktig bruker for å få tilgang til denne handlingen!");
            }

            if (post == null)
            {
                return NotFound();
            }

            ViewData["blogId"] = post.BlogId;
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PostId,Title,Content,BlogId")] Post post)
        {
            if (id != post.PostId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.update(post);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostExists(post.PostId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("/Index/" + post.BlogId);
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _repository.getPost(id);
            var isAuthorized = await authorizationService.AuthorizeAsync(User, post, new OperationAuthorizationRequirement { Name = "delete" });

            if (!isAuthorized.Succeeded)
            {
                return View("Du må være innlogget på riktig bruker for å få tilgang til denne handlingen!");
            }

            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var post = _repository.getPost(id);
            _repository.Delete(id);
            return RedirectToAction("Index/" + post.BlogId, "posts");
        }

        private bool PostExists(int id)
        {
            return _repository.getPost(id) != null;
        }
    }
}
