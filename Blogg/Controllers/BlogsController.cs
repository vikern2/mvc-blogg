﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blogg.Models;
using Blogg.Repository;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Blogg.Controllers
{
    public class BlogsController : Controller
    { 
        private IRepository _repository;
        private IAuthorizationService authorizationService;

        public BlogsController(IRepository repository, IAuthorizationService authorizationService)
        {
            this.authorizationService = authorizationService;
            _repository = repository;
        }

        // GET: Blogs
        public async Task<IActionResult> Index()
        {
            var allBlogs = _repository.getall();
            List<BlogViewModel> allBlogsViewModel = new List<BlogViewModel>();
            for(int i = 0; i < allBlogs.Count(); i++)
            {
                Blog blog = allBlogs.ElementAt(i);
                BlogViewModel model = new BlogViewModel
                {
                    Name = blog.Name,
                    Owner = blog.Owner,
                    CommentCount = _repository.CountComments(blog.BlogId),
                    BlogId = blog.BlogId
                };
                allBlogsViewModel.Add(model);
            }

            allBlogsViewModel.Sort((y,x) => x.CommentCount.CompareTo(y.CommentCount));
            return View(allBlogsViewModel);
        }

        // GET: Blogs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blog = _repository.getBlog(id);
            if (blog == null)
            {
                return NotFound();
            }

            return View(blog);
        }

        [Authorize]
        // GET: Blogs/Create
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        // POST: Blogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BlogId,Owner,Name")] Blog blog)
        {
            ClaimsPrincipal userClaims = this.User; 

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.createAsync(blog, userClaims).Wait();
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View(blog);
                }
            }
            return View(blog);
        }

        // GET: Blogs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var blog = _repository.getBlog(id);

            var isAuthorized = await authorizationService.AuthorizeAsync(User, blog, new OperationAuthorizationRequirement { Name = "edit" });

            if (!isAuthorized.Succeeded)
            {
                return View("Du må være innlogget på riktig bruker for å få tilgang til denne handlingen!");
            }

            if (blog == null)
            {
                return NotFound();
            }
            return View(blog);
        }

        // POST: Blogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BlogId,Owner,Name")] Blog blog)
        {
            if (id != blog.BlogId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.update(blog);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BlogExists(blog.BlogId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(blog);
        }

        // GET: Blogs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blog = _repository.getBlog(id);

            var isAuthorized = await authorizationService.AuthorizeAsync(User, blog, new OperationAuthorizationRequirement { Name = "delete" });

            if (!isAuthorized.Succeeded)
            {
                return View("Du må være innlogget på riktig bruker for å få tilgang til denne handlingen!");
            }

            if (blog == null)
            {
                return NotFound();
            }

            return View(blog);
        }

        // POST: Blogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _repository.delete(id);
            return RedirectToAction(nameof(Index));
        }

        private bool BlogExists(int id)
        {
            return _repository.getBlog(id) != null;
        }
    }
}
