﻿using System;
using System.Collections.Generic;
using System.Text;
using Blogg.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace Blogg.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Blog> blogs { get; set; }
        public virtual DbSet<Post> posts { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
    }
}
