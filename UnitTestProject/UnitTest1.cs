using Blogg.Controllers;
using Blogg.Models;
using Blogg.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        Mock<IPostRepository> _repository;
        Mock<ICommentRepository> _cRepository;
        Mock<IRepository> _bRepository;

        /*[TestMethod]
        public async System.Threading.Tasks.Task DeleteConfirmedCallsDeleteInIRepositoryAsync()
        {
            _repository = new Mock<IPostRepository>();

            Post post = new Post { Title = "asd", Content = "dsa", BlogId = 0, PostId = 0 };
            _repository.Setup(x => x.getPost(0)).Returns(post);

            var controller = new PostsController(_repository.Object);
            await controller.DeleteConfirmed(0);

            _repository.Verify(x => x.Delete(post.PostId));
        }

        [TestMethod]
        public void getPostReturnsCorrectPost()
        {
            _repository = new Mock<IPostRepository>();
            List<Post> posts = new List<Post>
            {
                
                new Post {Title="qwe", Content="rty", BlogId=0}
            };
            _repository.Setup(x => x.getPost(0)).Returns(new Post { Title = "asd", Content = "dsa", BlogId = 0 });

            var controller = new PostsController(_repository.Object);

            Assert.AreEqual("asd", _repository.Object.getPost(0).Title, "Wrong title of post");

        }

        [TestMethod]
        public void getCommentReturnsCorrectComment()
        {
            _cRepository = new Mock<ICommentRepository>();
            Comment comment = new Comment { Author = "Erik", CommentId = 1, Content = "asd" };
            _cRepository.Setup(x => x.getComment(1)).Returns(comment);

            var controller = new CommentsController(_cRepository.Object);

            Assert.AreEqual("asd", _cRepository.Object.getComment(1).Content, "Wrong title of post");

        }

        [TestMethod]
        public void PostInGetCommentReturnsCorrectPost()
        {
            _cRepository = new Mock<ICommentRepository>();
            Post post = new Post { Title = "asd", Content = "dsa", BlogId = 0, PostId = 0 };
            Comment comment = new Comment { Author = "Erik", CommentId = 1, Content = "asd", Post=post, PostId=0 };
            _cRepository.Setup(x => x.getComment(1)).Returns(comment);

            var controller = new CommentsController(_cRepository.Object);

            Assert.AreEqual(post, _cRepository.Object.getComment(1).Post, "Got wrong post");
        }

        [TestMethod]
        public void getBlogReturnsCorrectBlog()
        {
            _bRepository = new Mock<IRepository>();
            _bRepository.Setup(x => x.getBlog(0)).Returns(new Blog { Owner = "Erik", BlogId = 0, Name = "Bloggen" });
            var controller = new BlogsController(_bRepository.Object);
            
            Assert.AreEqual("Erik", _bRepository.Object.getBlog(0).Owner, "Got wrong blog");
        }*/

    }
}
